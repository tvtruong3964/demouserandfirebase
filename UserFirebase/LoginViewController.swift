//
//  LoginViewController.swift
//  DemoFirebase
//
//  Created by Truong Tran on 12/7/17.
//  Copyright © 2017 Truong Tran. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
  

  
  // MARK: Outlets
  @IBOutlet weak var textFieldLoginEmail: UITextField!
  @IBOutlet weak var textFieldLoginPassword: UITextField!
  
  lazy var usersRef = FIRDatabase.database().reference(withPath: "demo-user").child("user")
  var currentUserRef: FIRDatabaseReference?

  override func viewDidLoad() {
    super.viewDidLoad()

  }
  
  // MARK: Actions
  @IBAction func loginDidTouch(_ sender: AnyObject) {
    FIRAuth.auth()?.signIn(withEmail: textFieldLoginEmail.text!, password: textFieldLoginPassword.text!)          {
      firUser, error in
      if error == nil {
        print("***Login Success")
        
        let alert = UIAlertController(title: "Login", message: "Login Success", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)

        
        
      } else {
        print("***Login error \(String(describing: error))")
        
        let alert = UIAlertController(title: "Login", message: "Login error", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
      }
      
    }
    
  }
  
  @IBAction func signUpDidTouch(_ sender: AnyObject) {
    
    
    
    let alert = UIAlertController(title: "Register",
                                  message: "Register",
                                  preferredStyle: .alert)
    
    let saveAction = UIAlertAction(title: "Save",style: .default) {
      action in
      let emailField = alert.textFields![0]
      let userNameField = alert.textFields![1]
      let phoneField = alert.textFields![2]
      let passwordField = alert.textFields![3]
      
      FIRAuth.auth()!.createUser(withEmail: emailField.text!, password: passwordField.text!)
      {
        user, error in
        if error == nil {
          print("***Sigin Success")
          let currentUserRef = self.usersRef.child(user!.uid) // set key dictionary
          currentUserRef.setValue(["email": emailField.text, "username": userNameField.text, "phone": phoneField.text])
          
          let alert = UIAlertController(title: "Login", message: "Sigin Success", preferredStyle: UIAlertControllerStyle.alert)
          alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
          
        } else {
          print("***sigin error \(String(describing: error))")
          
          let alert = UIAlertController(title: "Login", message: "Sigin Error", preferredStyle: UIAlertControllerStyle.alert)
          alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
          self.present(alert, animated: true, completion: nil)
        }
      }
    }
    
    let cancelAction = UIAlertAction(title: "Cancel",
                                     style: .default)
    
    alert.addTextField { textEmail in
      textEmail.placeholder = "Enter your email"
    }
    alert.addTextField { userNameField in
      userNameField.placeholder = "Enter your user name"
    }
    alert.addTextField { phoneField in
      phoneField.placeholder = "Enter your phone"
    }
    
    alert.addTextField { textPassword in
      textPassword.isSecureTextEntry = true
      textPassword.placeholder = "Enter your password"
    }
    
    alert.addAction(saveAction)
    alert.addAction(cancelAction)
    
    present(alert, animated: true, completion: nil)
  }
  
  
  
}

